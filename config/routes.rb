Rails.application.routes.draw do
 
   # Routing 
   resources :users
   get 'signup', to: 'users#new', as: 'signup'
   get 'login', to: 'sessions#new', as: 'login'
   get 'afterlog', to: 'users#afterlog', as: 'afterlog'
   get 'home', to: 'users#home', as: 'home'

   # Root URL 
   root to: 'sessions#new'

   #For Logging in 
   post 'login', to: 'sessions#create'

   # Destroying  session , Logging Out
   delete 'logout', to: 'sessions#destroy', as: 'logout'


  
end
